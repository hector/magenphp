<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 *
 * @method \App\Model\Entity\Category[] paginate($object = null, array $settings = [])
 */
class CategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $categories = $this->paginate($this->Categories);

        $this->set(compact('categories'));
        $this->set('_serialize', ['categories']);
    }

    /**
     * View method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $category = $this->Categories->get($id, [
            'contain' => ['Prayers']
        ]);

        $this->set('category', $category);
        $this->set('_serialize', ['category']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $category = $this->Categories->newEntity();
        if ($this->request->is('post')) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
        }
        $prayers = $this->Categories->Prayers->find('list', ['limit' => 200]);
        $this->set(compact('category', 'prayers'));
        $this->set('_serialize', ['category']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $category = $this->Categories->get($id, [
            'contain' => ['Prayers']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
        }
        $prayers = $this->Categories->Prayers->find('list', ['limit' => 200]);
        $this->set(compact('category', 'prayers'));
        $this->set('_serialize', ['category']);
    }

    public function order() {
        $query = $this->Categories->find()
            //->contain(['Categories'])
            ->order(['Categories.position' => "ASC"]);

        $categories = $this->paginate($query,["limit" =>1000,"maxLimit" => 1000]);
        $this->set(compact('categories'));
        $this->set('_serialize', ['categories']);
    }

    function saveOrder(){

        if($this->request->is('post')){
            $order = 0;
            foreach($this->request->getData("data") as $category){
                $p = $this->Categories->get($category["id"]);
                $p = $this->Categories->patchEntity($p, ['position' => ++$order]);
                if($this->Categories->save($p)){
                    $result = 'saved';
                }else{
                    $result = 'fail';
                    break;
                }
            }
        }else{
            $result = 'only XHR for this method';
        }


        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }
}
