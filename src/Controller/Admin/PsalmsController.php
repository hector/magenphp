<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Psalms Controller
 *
 * @property \App\Model\Table\PsalmsTable $Psalms
 *
 * @method \App\Model\Entity\Psalm[] paginate($object = null, array $settings = [])
 */
class PsalmsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $query = $this->Psalms->find()
            ->order(["Psalms.name" =>"ASC"]);

        if($this->request->getQuery("search")){
            $search = $this->request->getQuery("search");
            $query->where([
                    'OR' => [
                        'Psalms.id' => $search,
                        'Psalms.name LIKE' => $search,
                    ]
            ]);
        }

        $psalms = $this->paginate($query)->toArray();
//
        $this->set(compact('psalms'));
        $this->set('_serialize', ['psalms']);
    }

    /**
     * View method
     *
     * @param string|null $id Psalm id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $psalm = $this->Psalms->get($id, [
            'contain' => []
        ]);

        $this->set('psalm', $psalm);
        $this->set('_serialize', ['psalm']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $psalm = $this->Psalms->newEntity();
        if ($this->request->is('post')) {
            $psalm = $this->Psalms->patchEntity($psalm, $this->request->getData());
            if ($this->Psalms->save($psalm)) {
                $this->Flash->success(__('The psalm has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The psalm could not be saved. Please, try again.'));
        }
        $this->set(compact('psalm'));
        $this->set('_serialize', ['psalm']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Psalm id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $psalm = $this->Psalms->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $psalm = $this->Psalms->patchEntity($psalm, $this->request->getData());
            if ($this->Psalms->save($psalm)) {
                $this->Flash->success(__('The psalm has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The psalm could not be saved. Please, try again.'));
        }
        $this->set(compact('psalm'));
        $this->set('_serialize', ['psalm']);
    }
}
