<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * PaymentsValues Controller
 *
 * @property \App\Model\Table\PaymentsValuesTable $PaymentsValues
 *
 * @method \App\Model\Entity\PaymentsValue[] paginate($object = null, array $settings = [])
 */
class PaymentsValuesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        if ($this->request->is('post')) {

            $entities = $this->PaymentsValues->newEntities($this->request->getData("PaymentsValues"));
            if($this->PaymentsValues->saveMany($entities)) {
                $this->Flash->success(__('The Payments values has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(
                __('The Payments values could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }

        $paymentsValues = $this->paginate($this->PaymentsValues);

        $this->set(compact('paymentsValues'));
        $this->set('_serialize', ['paymentsValues']);
    }

}
