<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Exception\Exception;
use Cake\ORM\TableRegistry;

/**
 * Prayers Controller
 *
 * @property \App\Model\Table\PrayersTable $Prayers
 *
 * @method \App\Model\Entity\Prayer[] paginate($object = null, array $settings = [])
 */
class PrayersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $query = $this->Prayers->find()
            ->contain(['Categories'])
            ->order(['Prayers.position' => "ASC"]);

        if($this->request->getQuery("search")){
            $search = $this->request->getQuery("search");
            $query->where([
                'OR' => [
                    'Prayers.title LIKE ' => "%".$search."%",
                ]
            ]);

        }

        $prayers = $this->paginate($query);
        $this->set(compact('prayers'));
        $this->set('_serialize', ['prayers']);
    }

    /**
     * View method
     *
     * @param string|null $id Prayer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $prayer = $this->Prayers->get($id, [
            'contain' => []
        ]);

        $this->set('prayer', $prayer);
        $this->set('_serialize', ['prayer']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $prayer = $this->Prayers->newEntity();
        if ($this->request->is('post')) {
            $prayer = $this->Prayers->patchEntity($prayer, $this->request->getData());
            if ($this->Prayers->save($prayer)) {
                $this->Flash->success(__('The prayer has been saved.'));

                return $this->redirect(['action' => 'edit',$prayer->get("id")]);
            }
            $this->Flash->error(__('The prayer could not be saved. Please, try again.'));
        }
        $types = $this->Prayers->getTypes();

        $categories = $this->Prayers->Categories->find('list', ['limit' => 200,
            'keyField' => 'id',
            'valueField' => 'title'
        ]);

        $this->set(compact('prayer','types','categories'));
        $this->set('_serialize', ['prayer','types','categories']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Prayer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $prayer = $this->Prayers->get($id, [
            'contain' => ['Categories']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $prayer = $this->Prayers->patchEntity($prayer, $this->request->getData());
            if ($this->Prayers->save($prayer)) {
                $this->Flash->success(__('The prayer has been saved.'));

                return $this->redirect(['action' => 'edit',$id]);
            }
            $this->Flash->error(__('The prayer could not be saved. Please, try again.'));
        }
        $types = $this->Prayers->getTypes();

        $categories = $this->Prayers->Categories->find('list', ['limit' => 200,
            'keyField' => 'id',
            'valueField' => 'title'
        ]);

        $this->set(compact('prayer','types','categories'));
        $this->set('_serialize', ['prayer','types','categories']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Prayer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $prayer = $this->Prayers->get($id);
        if ($this->Prayers->delete($prayer)) {
            $this->Flash->success(__('The prayer has been deleted.'));
        } else {
            $this->Flash->error(__('The prayer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function order() {
        $query = $this->Prayers->find()
            ->order(['Prayers.title' => "ASC"]);

        $categories = $this->Prayers->Categories
            ->find()
            ->contain(["Prayers" => [
                    'sort' => ['CategoriesPrayers.position' => 'ASC']
                ]
            ])
            ->order(["Categories.position" => "ASC"])
            ->all();

        $prayers = $this->paginate($query,["limit" =>1000,"maxLimit" => 1000]);
        $this->set(compact('prayers','categories'));
        $this->set('_serialize', ['prayers','categories']);
    }

    function saveOrder(){

            $data = $this->request->getData("data");
            $order = 0;
            $category = $this->Prayers->Categories->get($data["category_id"],[ 'contain' => ['Prayers']]);

            $CategoriesPrayers = TableRegistry::get("CategoriesPrayers");

            foreach($data["ids"] as $prayer_id){

                foreach($category->prayers as $prayer){
                    if($prayer->id == $prayer_id){
                        $cat_pra = $CategoriesPrayers->get($prayer->_joinData->id);
                        $cat_pra->position = ++$order;
                        $result = $CategoriesPrayers->save($cat_pra);
                        if($result){
                            $result = 'saved';
                        }else{
                           $result = 'fail';
                        }
                        break;
                    }
                }
            }

        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }

    function testLink(){
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $type = $this->request->getQuery("type");
        $link = $this->request->getQuery("link");
        $result = false;
        if(in_array($type,["pdf","imagelink"])){
            try{
                $result = @file_get_contents($link,0,stream_context_create($arrContextOptions),0,1);
            }catch(Exception $e){
                $result = false;
            }
            if (false === $result) {
                echo "0";
            }else{
                echo "1";
            }
        }

        $this->render(false,false);

    }
}
