<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Texts Controller
 *
 * @property \App\Model\Table\TextsTable $Texts
 *
 * @method \App\Model\Entity\Text[] paginate($object = null, array $settings = [])
 */
class TextsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $texts = $this->paginate($this->Texts);

        $this->set(compact('texts'));
        $this->set('_serialize', ['texts']);
    }

    /**
     * View method
     *
     * @param string|null $id Text id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $text = $this->Texts->get($id, [
            'contain' => []
        ]);

        $this->set('text', $text);
        $this->set('_serialize', ['text']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $text = $this->Texts->newEntity();
        if ($this->request->is('post')) {
            $text = $this->Texts->patchEntity($text, $this->request->getData());
            if ($this->Texts->save($text)) {
                $this->Flash->success(__('The text has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The text could not be saved. Please, try again.'));
        }
        $this->set(compact('text'));
        $this->set('_serialize', ['text']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Text id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $text = $this->Texts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $text = $this->Texts->patchEntity($text, $this->request->getData());
            if ($this->Texts->save($text)) {
                $this->Flash->success(__('The text has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The text could not be saved. Please, try again.'));
        }
        $this->set(compact('text'));
        $this->set('_serialize', ['text']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Text id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $text = $this->Texts->get($id);
        if ($this->Texts->delete($text)) {
            $this->Flash->success(__('The text has been deleted.'));
        } else {
            $this->Flash->error(__('The text could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
