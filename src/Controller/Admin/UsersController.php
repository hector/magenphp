<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public $paginate = [
        'limit' => 20,
        'order' => [
            'Users.id' => 'DESC'
        ]
    ];

    function login(){
        $this->viewBuilder()->setLayout('login');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('Invalid Credentials'), [
                    'key' => 'auth'
                ]);
            }
        }
    }

    public function logout()
    {
        $this->Flash->success('Good Bye');
        return $this->redirect($this->Auth->logout());
    }

    function add(){
        $users = TableRegistry::get('Users');
        if ($this->request->is('post')) {
            $user = $users->newEntity($this->request->getData());
            if ($users->save($user)) {
                $this->Flash->success(__('User Created'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('User cannot be created'), []);
        }else{
            $user = $users->newEntity();
        }
        $this->set(compact("user"));
    }

    function edit($id){
        $user = $this->Users->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('User updated'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Cannot update user'));
        }

        $this->set('user', $user);
    }
    public function profile()
    {
        $user = $this->Users->get($this->Auth->user("id"));
        //$user = $this->Users->get(4);
        if ($this->request->is(['post', 'put'])) {
            $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('User updated'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Cannot update user'));
        }

        $this->set('user', $user);
    }

    function index(){
        $rows = $this->paginate($this->Users);
        $this->set(compact('rows'));
    }
    public function changePassword()
    {
        $user = $this->Users->get($this->Auth->user("id"));
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, [
                'old_password'  => $this->request->getData('old_password'),
                'password'      => $this->request->getData('password1'),
                'password1'     => $this->request->getData('password1'),
                'password2'     => $this->request->getData('password2')
            ],
                ['validate' => 'password']
            );

            if ($this->Users->save($user)) {
                $this->Flash->success(__('Password updated.'));

                return $this->redirect(['action' => 'profile']);
            }
            $this->Flash->error(__('The password couldn\'t be upated. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

    }
}
