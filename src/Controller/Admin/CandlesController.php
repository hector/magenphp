<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Candles Controller
 *
 * @property \App\Model\Table\CandlesTable $Candles
 *
 * @method \App\Model\Entity\Candle[] paginate($object = null, array $settings = [])
 */
class CandlesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $query = $this->Candles->find()
            ->contain(["Payments","Prayers"])
            ->order(["Candles.created" =>"DESC"]);

        if($this->request->getQuery("search")){
            $search = $this->request->getQuery("search");
            $query->where([
                'OR' => [
                    'Candles.email' => $search,
                    'Candles.name LIKE' => $search,
                    'Candles.parent_name LIKE' => $search,
                ]
            ]);
        }

        $candles = $this->paginate($query);
//
        $this->set(compact('candles'));
        $this->set('_serialize', ['candles']);
    }

    /**
     * View method
     *
     * @param string|null $id Candle id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $candle = $this->Candles->get($id, [
            'contain' => ['Payments']
        ]);

        $this->set('candle', $candle);
        $this->set('_serialize', ['candle']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $candle = $this->Candles->newEntity();
        if ($this->request->is('post')) {
            $candle = $this->Candles->patchEntity($candle, $this->request->getData());
            if ($this->Candles->save($candle)) {
                $this->Flash->success(__('The candle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The candle could not be saved. Please, try again.'));
        }
        $payments = $this->Candles->Payments->find('list', ['limit' => 200]);
        $prayers = $this->Candles->Prayers->find('list', ['limit' => 200]);
        $donations = TableRegistry::get("PaymentsValues")->find("list",[
            'keyField' => 'days',
            'valueField' => function ($donation) {
                return $donation->get('label');
            }
        ]);
        $this->set(compact('candle', 'payments', 'prayers','donations'));
        $this->set('_serialize', ['candle']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Candle id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $candle = $this->Candles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $candle = $this->Candles->patchEntity($candle, $this->request->getData());
            if ($this->Candles->save($candle)) {
                $this->Flash->success(__('The candle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The candle could not be saved. Please, try again.'));
        }

        $payments = $this->Candles->Payments->find('list', ['limit' => 200]);
        $prayers = $this->Candles->Prayers->find('list', ['limit' => 200]);
        $donations = TableRegistry::get("PaymentsValues")->find("list",[
            'keyField' => 'days',
            'valueField' => function ($donation) {
                return $donation->get('label');
            }
        ]);
        $this->set(compact('candle', 'payments','prayers','donations'));
        $this->set('_serialize', ['candle']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Candle id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $candle = $this->Candles->get($id);
        if ($this->Candles->delete($candle)) {
            $this->Flash->success(__('The candle has been deleted.'));
        } else {
            $this->Flash->error(__('The candle could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
