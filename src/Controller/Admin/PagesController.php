<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Pages Controller
 *
 *
 */
class PagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

    }

    function uploadEditor(){
        if ($this->request->is('post')) {
            if ($_FILES['upload']) {
                $image = $_FILES['upload'];
                //allowed image types
                $imageTypes = array("image/gif", "image/jpeg", "image/png");
                //upload folder - make sure to create one in webroot
                $uploadFolder = "files".DS."uploads";
                //full path to upload folder
                $uploadPath = WWW_ROOT . $uploadFolder;


                //check if image type fits one of allowed types
                if(in_array($image['type'],$imageTypes)){

                    //check if there wasn't errors uploading file on serwer
                    if ($image['error'] == 0) {
                        //image file name
                        $imageName = date('His');
                        $orname = explode(".",$image["name"]);

                        $full_image_path = $uploadPath . '/' . $imageName.".".$orname[1];
                        $orname = $imageName.".".$orname[1];
                        //upload image to upload folder

                        if (move_uploaded_file($image['tmp_name'], $full_image_path)) {
                            $url = \Cake\Routing\Router::url("/",true)."files/uploads/".$orname;
                            $message = '';

                            if($this->request->getQuery("met") && $this->request->getQuery("met") =='drop'){
                                echo json_encode(
                                    [
                                        "uploaded"=> 1,
                                        "fileName"=> $orname,
                                        "url"=> $url
                                    ]
                                );
                                die();
                            }

                            $funcNum = $_GET['CKEditorFuncNum'] ;
                            echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction('$funcNum', '$url', '$message');</script>";
                            exit;
                        } else {
                            die();
                        }
                    } else {
                        die("incorrect file extension");
                    }
                }
            }
        }
        die();
    }

}
