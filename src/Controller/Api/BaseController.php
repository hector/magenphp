<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Base Controller
 */
class BaseController extends AppController
{

    public function initialize() {
        parent::initialize();
        $this->modelClass = false;
    }
    public function index()
    {
        $base = [
            'result' => true
        ];

        $this->set(compact('base'));
        $this->set('_serialize', ['base']);
        $this->render(false);
    }

}
