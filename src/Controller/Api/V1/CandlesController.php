<?php
namespace App\Controller\Api\V1;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Candles Controller
 *
 * @property \App\Model\Table\CandlesTable $Candles
 *
 * @method \App\Model\Entity\Candle[] paginate($object = null, array $settings = [])
 */
class CandlesController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent("Stripe");
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Payments']
        ];
        $candles = $this->paginate($this->Candles);

        $this->set(compact('candles'));
        $this->set('_serialize', ['candles']);
    }

   function buyCandle(){

       if($this->request->is("post")){
           $data = $this->request->getData("data");

           $price = TableRegistry::get("PaymentsValues")->find()
                ->where(["PaymentsValues.days" => $data["Candle"]["donation"]])
                ->first()
                ->get("price");

           $data["cc"]["amount"] = $price;
           $result = $this->Stripe->create($data);
           if($result["result"]){
               $result["message"] = "";
               $data["Payments"]["amount"] = $result["payment"]->amount/100;
               $data["Payments"]["payment_identifier"] = $result["payment"]->id;
               $data["Payments"]["status"] = $result["payment"]->status;
               $PaymentsTable = TableRegistry::get("Payments");
               $payment = $PaymentsTable->newEntity($data["Payments"]);
               if($PaymentsTable->save($payment)) {
                   $data["Candle"]["payment_id"] = $payment->get("id");
                   $candle = $this->Candles->newEntity($data["Candle"]);
                   if($this->Candles->save($candle)) {
                       $result["data"]["candle"] = $candle;
                       $result["message"] = "The candle has been saved";
                   }else{
                       $result["message"] = "The payment has been processed but the candle couldn't be saved";
                   }
               }else{
                   $result["message"] = "The payment couldn't be saved";
               }

               unset($result["payment"]);
           }
           $this->set(compact('result'));
           $this->set('_serialize', ['result']);
       }
   }


    function getByPrayer($prayer_id = 0){

        $candles = $this->Candles->find("byPrayer",["prayer_id" => $prayer_id])
            //->enableHydration(treue)
            ->toArray();


        $result = [
            "success" =>true,
            "data" => $candles
        ];

        $this->set(compact('result'));
        $this->set('_serialize', ['result']);

    }




}
