<?php
namespace App\Controller\Api\V1;

use App\Controller\AppController;

/**
 * Prayers Controller
 *
 * @property \App\Model\Table\PrayersTable $Prayers
 * @property \App\Model\Table\CategoriesTable $Categories
 *
 * @method \App\Model\Entity\Prayer[] paginate($object = null, array $settings = [])
 */
class PrayersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $prayers = $this->paginate($this->Prayers);

        $this->set(compact('prayers'));
        $this->set('_serialize', ['prayers']);
    }

    /**
     * View method
     *
     * @param string|null $id Prayer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $prayer = $this->Prayers->find()
            ->where(["Prayers.id" => $id])
            ->contain(['Categories'])
            ->first();


        $result = [
            "success" =>true,
            "data" => $prayer
        ];


        $this->set('result', $result);
        $this->set('_serialize', ['result']);
    }

    public function categorized(){
        $result = $this->Prayers->Categories
            ->find()
            ->enableHydration(false)
            ->contain(["Prayers"  => function ($q) {
                    return $q
                        ->select([
                            'Prayers.id',
                            'Prayers.title',
                            'Prayers.description',
                            'Prayers.paid',
                        ])
                        ->order(['CategoriesPrayers.position' => 'ASC']);
                }
            ])
            ->order(["Categories.position" => "ASC"])
            ->all();

        $result = [
            "success" =>true,
            "data" => $result
        ];
        $this->set('result', $result);

        $this->set('_serialize', ['result']);
    }
}
