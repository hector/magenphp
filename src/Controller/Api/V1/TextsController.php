<?php
namespace App\Controller\Api\V1;

use App\Controller\AppController;

/**
 * Texts Controller
 *
 * @property \App\Model\Table\TextsTable $Texts
 *
 * @method \App\Model\Entity\Text[] paginate($object = null, array $settings = [])
 */
class TextsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $texts = $this->paginate($this->Texts);

        $this->set(compact('texts'));
        $this->set('_serialize', ['texts']);
    }

    /**
     * View method
     *
     * @param string|null $id Text id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $result = $this->Texts->find()
            ->where(['OR' => ["Texts.abbrev" => $id,"Texts.id" => $id]])
            ->first();

        $text = [
            "success" => true,
            "data" =>  $result
        ];
        $this->set('text', $text);
        $this->set('_serialize', ['text']);
    }
}
