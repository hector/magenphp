<?php
namespace App\Controller\Api\V1;

use App\Controller\AppController;

/**
 * PaymentsValues Controller
 *
 * @property \App\Model\Table\PaymentsValuesTable $PaymentsValues
 *
 * @method \App\Model\Entity\PaymentsValue[] paginate($object = null, array $settings = [])
 */
class PaymentsValuesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $result = $this->PaymentsValues
            ->find()
            ->toArray();

        $result = [
            "success" =>true,
            "data" => $result
        ];
        $this->set('result', $result);
    }

}
