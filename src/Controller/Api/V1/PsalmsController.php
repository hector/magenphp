<?php
namespace App\Controller\Api\V1;

use App\Controller\AppController;

/**
 * Psalms Controller
 *
 * @property \App\Model\Table\PsalmsTable $Psalms
 *
 * @method \App\Model\Entity\Psalm[] paginate($object = null, array $settings = [])
 */
class PsalmsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $result = $this->Psalms
            ->find()
            ->limit(13);

        if($this->request->getQuery("from")){
            $result->where(["Psalms.id > " => $this->request->getQuery("from")]);
        }
        $result = $result->toArray();
        $result = [
            "success" =>true,
            "data" => $result
        ];

        $this->set('result', $result);

    }

    /**
     * View method
     *
     * @param string|null $id Psalm id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $psalm = $this->Psalms->get($id, [
            'contain' => []
        ])->toArray();

        $result = [
            "success" =>true,
            "data" => $psalm
        ];
        $this->set('result', $result);
        $this->set('_serialize', ['result']);
    }

    public function filter(){
        $result = $this->Psalms
            ->find()
            ->limit(13);

        $psalm_id = $this->request->getQuery("psalm_id");

        if($psalm_id){
            $result->where(["Psalms.id" =>$psalm_id]);
        }

        $result = $result->toArray();
        $result = [
            "success" =>true,
            "data" => $result
        ];
        $this->set('result', $result);
    }

    public function chapters()
    {
        $result = $this->Psalms
            ->find()
            ->select(["Psalms.id","Psalms.name"])
            ->limit(500);


        $result = $result->toArray();
        $result = [
            "success" =>true,
            "data" => $result
        ];
        $this->set('result', $result);

    }

    public function next($id = null)
    {
        $result = $this->Psalms
            ->find()
            ->where(["Psalms.id >" => $id])
            ->first();


        if(empty($result)){//last record
            $result = $this->Psalms
                ->find()
                ->order(["Psalms.id" => "ASC"])
                ->first();

        }

        $result = [
            "success" =>true,
            "data" => $result
        ];
        $this->set('result', $result);
        $this->set('_serialize', ['result']);
    }

    public function prev($id = null)
    {
        $result = $this->Psalms
            ->find()
            ->where(["Psalms.id <" => $id])
            ->order(["Psalms.id" => 'DESC'])
            ->first();


        if(empty($result)){//last record
            $result = $this->Psalms
                ->find()
                ->order(["Psalms.id" => 'DESC'])
                ->first()
                ->toArray();
        }

        $result = [
            "success" =>true,
            "data" => $result
        ];
        $this->set('result', $result);
        $this->set('_serialize', ['result']);

    }
}
