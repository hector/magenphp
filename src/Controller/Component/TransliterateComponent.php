<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Transliterate component
 */
class TransliterateComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];


    static function GetOneSignArray()
    {
        $arr = array(
            "א" => "a",
            "ב" => "b",
            "ג" => "c",
            "ד" => "d",
            "ה" => "e",
            "ו" => "f",
            "ז" => "g",
            "ח" => "h",
            "ט" => "i",
            "י" => "j",
            "כ" => "k",
            "ך" => "k",
            "ל" => "l",
            "מ" => "m",
            "ם" => "m",
            "נ" => "n",
            "ן" => "n",
            "ס" => "o",
            "ע" => "p",
            "פ" => "q",
            "ף" => "q",
            "צ" => "r",
            "ץ" => "r",
            "ק" => "s",
            "ר" => "t",
            "ש" => "u",
            "ת" => "v",
            "ׂ" => "A",
            "ׁ" => "B",
            "ְ" => "C",
            "ָ" => "D",
            "ַ" => "E",
            "ֶ" => "F",
            "ֵ" => "G",
            "ִ" => "H",
            "ֹ" => "I",
            "ֻ" => "J",
            "ֳ" => "K",
            "ֲ" => "L",
            "ֱ" => "M",
            "ּ" => "N",
        );

        return $arr;
    }


    static function ReplaceExpWords($text)
    {

        $text = str_replace("לא", "לֹא", $text);
        $text = str_replace("יְיָ", "אֲדֹנָי ", $text);
        $text = str_replace("יוֹ'ד", "יוֹד", $text);
        $text = str_replace("כָּל", "קוֹל ", $text);
        $text = str_replace("לַיהֹוָה", "לַּאֲדֹנָי", $text);
        $text = str_replace("לַיְהֹוָה", "לַּאֲדֹנָי", $text);
        $text = str_replace("משֶׁה", "מֹשֶׁה", $text);
        $text = str_replace("י'", " יוֹד", $text);
        $text = str_replace("ה'", "'הֶא", $text);
        $text = str_replace("ח'י", "חַי", $text);
        $text = str_replace("יְדוָֹ'ד", "אֲדֹנָי", $text);

        return $text;

    }


    static function ConvertStr2OneSign($str)
    {

        $str = self::ReplaceExpWords($str);
        $arr = self::GetOneSignArray();

        foreach ($arr as $key => $value) {
            $str = str_replace($key, $value, $str);
        }
        return $str;
    }


    static function ParseLetters($oneSignText)
    {
        $text = $oneSignText;
        $resultArray = array();
        $signsBlock = "";
        $index = 0;
        $arrayIndex = 0;

        while ($index < strlen($text)) {
            $ch = $text[$index];
            $pattern = '/[a-v]/'; // hebrew letters
            if (preg_match($pattern, $ch, $matches)) {
                if ($signsBlock != "") {
                    $resultArray[$arrayIndex] = $signsBlock;
                    $arrayIndex++;
                    $signsBlock = "";
                }

                $nikud = self::GetNikudForLetter($ch, $text, $index + 1);
                $dagesh = self::GetDageshForLetter($ch, $text, $index + 1);
                if (($ch == "f") && ($nikud == "no_nikud") && ($dagesh != "no_dagesh")) // vav + dagesh & no other nikud -> actually shuruk. & no Dagesh
                {
                    $nikud = "N";
                    $dagesh = "no_dagesh";
                }

                $ot = $ch;
                if ($ch == "u") // ==> ש
                {
                    $shinLocation = self::GetNekudatShinLocation($text, $index + 1);
                    if ($shinLocation == "left")
                        $ot = "sin";
                }


                $baseOt = "Ot;ot_name;ot_nikud;ot_dagesh";
                $thisOt = str_replace("ot_name", $ot, $baseOt);
                $thisOt = str_replace("ot_nikud", $nikud, $thisOt);
                $thisOt = str_replace("ot_dagesh", $dagesh, $thisOt);


                $resultArray[$arrayIndex] = $thisOt;
                $arrayIndex++;
            } else {
                $pattern = '/[A-N]/'; // Skip Nikud Signs
                if (!preg_match($pattern, $ch, $matches)) {
                    $signsBlock = $signsBlock . $ch;
                }

            }

            $index++;
        }


        return $resultArray;
    }


    static function GetNekudatShinLocation($text, $index)
    {

        $shinLocation = "right";
        $searchIndex = $index;
        $count = 0;
        $currentSignIsHebrewLetter = false;

        while (($searchIndex < (strlen($text))) && (!$currentSignIsHebrewLetter) & ($count < 4)) {
            $currentChar = $text[$searchIndex];
            if ($currentChar == "A") // Left shin ==> sin
            {
                $shinLocation = "left";
                break;
            }

            $pattern = '/[a-v]/'; // hebrew letters
            if (preg_match($pattern, $currentChar, $matches))
                $currentSignIsHebrewLetter = true;

            $searchIndex++;
            $count++;

        }

        return $shinLocation;
    }


    static function GetNikudForLetter($ch, $text, $index)
    {
        $nikud = "no_nikud";
        $currentSignIsHebrewLetter = false;
        $searchIndex = $index;
        $count = 0;

        while (($searchIndex < (strlen($text))) && (!$currentSignIsHebrewLetter) & ($count < 4)) {
            $currentChar = $text[$searchIndex];
            $pattern = '/[C-M]/'; // nikud letters
            if (preg_match($pattern, $currentChar, $matches))
                $nikud = $currentChar;


            if ($nikud != "no_nikud")
                break;

            $pattern = '/[a-v]/'; // hebrew letters
            if (preg_match($pattern, $currentChar, $matches))
                $currentSignIsHebrewLetter = true;

            $searchIndex++;
            $count++;
        }
        return $nikud;
    }

    static function GetDageshForLetter($ch, $text, $index)
    {

        $dagesh = "no_dagesh";
        $pattern = '/[aehtp]/'; // hebrew letters without dagesh
        if (preg_match($pattern, $ch, $matches))
            return $dagesh;

        $currentSignIsHebrewLetter = false;
        $searchIndex = $index;
        $count = 0;

        while (($searchIndex < (strlen($text))) && (!$currentSignIsHebrewLetter) & ($count < 4)) {
            $currentChar = $text[$searchIndex];
            if ($currentChar == "N") //Dagesh Sign
            {
                $dagesh = "dagesh";
                break;
            }

            $pattern = '/[a-v]/'; // hebrew letters
            if (preg_match($pattern, $currentChar, $matches))
                $currentSignIsHebrewLetter = true;
            $searchIndex++;
            $count++;
        }

        return $dagesh;

    }

    static function IsOtItem($item)
    {
        // Ot Item In Array -> Start with "Ot"

        if (strlen($item) < 2)
            return false;
        if (($item[0] == "O") && ($item[1] == "t")) // This Item Is Ot
            return true;
        else
            return false;

    }


    static function ConvertToEnglish($lettersArray)
    {
        $finalText = "";
        $numOfLetters = sizeOf($lettersArray);

        for ($i = 0; $i < sizeof($lettersArray); $i++) {
            $currentStr = $lettersArray[$i];
            if (self::IsOtItem($currentStr)) // This Item Is Ot
            {
                $currentOt = $currentStr;
                $prevOt = "";
                $nextOt = "";
                $nextOt2 = "";

                if (($i > 0) && (self::IsOtItem($lettersArray[$i - 1])))
                    $prevOt = $lettersArray[$i - 1];

                if (($i < $numOfLetters - 1) && (self::IsOtItem($lettersArray[$i + 1]))) {
                    $nextOt = $lettersArray[$i + 1];
                }
                if (($i < $numOfLetters - 2) && (self::IsOtItem($lettersArray[$i + 2])))
                    $nextOt2 = $lettersArray[$i + 2];


                $targum = self::ParseOt2English($currentOt, $prevOt, $nextOt, $nextOt2);
                $finalText = $finalText . $targum;


            } else {
                $finalText = $finalText . $currentStr;
            }


        }

        return $finalText;
    }


    static function IsTenuaGedola($nikud)
    {

        if (($nikud == "D") || ($nikud == "G") || ($nikud == "I") || ($nikud == "N"))
            return true;
        else
            return false;


    }


    static function GetNikudLetter($nikud)
    {
        $nLetter = "";
        if (($nikud == "D") || ($nikud == "E") || ($nikud == "L"))
            $nLetter = "a";
        else
            if (($nikud == "+") || ($nikud == "I") || ($nikud == "K"))
                $nLetter = "o";
            else
                if (($nikud == ":") || ($nikud == "F") || ($nikud == "G") || ($nikud == "M"))
                    $nLetter = "e";

                else
                    if (($nikud == "J") || ($nikud == "N"))
                        $nLetter = "u";
                    else
                        if (($nikud == "H"))
                            $nLetter = "i";

        return $nLetter;


    }


    static  function ParseOt2English($otStr, $prevOtStr, $nextOtStr, $next2OtStr)
    {
        $englishStr = "";
        $otInfo = explode(";", $otStr);
        $otName = $otInfo[1];


        $nikud = $otInfo[2];
        $dagesh = $otInfo[3];
        $isDagesh = ($dagesh == "dagesh");
        $isFirstLetter = ($prevOtStr == "");

        $prevOtInfo = "";
        $nextOtInfo = "";
        $next2OtInfo = "";
        $nextOtName = "";
        $prevOtName ="";
        $prevNikud = "";

        if ($prevOtStr != "") {
            $prevOtInfo = explode(";", $prevOtStr);
            $prevOtName = $prevOtInfo[1];
            $prevNikud = $prevOtInfo[2];
            $prevDagesh = $prevOtInfo[3];
            $isPrevDagesh = ($prevDagesh == "dagesh");
        }


        if ($nextOtStr != "") {
            $nextOtInfo = explode(";", $nextOtStr);
            $nextOtName = $nextOtInfo[1];
            $nextNikud = $nextOtInfo[2];
            $nextDagesh = $nextOtInfo[3];
            $isNextDagesh = ($nextDagesh == "dagesh");

        }


        switch ($otName) {
            case "a":
                if (($prevOtName != "") && ($nextOtName != ""))
                    $englishStr = "'";
                break; // Only nikud
            case "b":
                if ($isDagesh)
                    $englishStr = "b";
                else
                    $englishStr = "v";
                break;

            case "c":
                $englishStr = "g";
                break;
            case "d":
                $englishStr = "d";
                break;
            case "e":
                //  if ( (nikud != "no_nikud") || ( (nextOtName =="ו") && ( (nextNikud ==cholam.ToString()) || (nextNikud == shuruk.ToString()))))
                $englishStr = "h";
                break;
            case "f":
                if (($nikud != "I") && ($nikud != "N")) // Cholam Or Shuruk
                    $englishStr = "v";
                // else add only nikud
                break;
            case "g":
                $englishStr = "z";
                if ($dagesh == "dagesh")
                    $englishStr = "zz";
                break;
            case "h":
                $englishStr = "ch";
                break;
            case "i":
                $englishStr = "t";
                if ($dagesh == "dagesh")
                    $englishStr = "tt";

                break;
            case "j":

                if (($prevNikud == "H") || (($nextOtName == "f") && ($next2OtStr == ""))) {

                    $englishStr = "";
                } else
                    $englishStr = "y";
                if (($prevNikud == "F") || ($prevNikud == "G") || ($prevNikud == "M"))
                    $englishStr = "i";

                break;
            case "k":
                if ($dagesh != "dagesh")
                    $englishStr = "ch";
                else
                    $englishStr = "k";
                break;
            case "l":
                $englishStr = "l";
                if ($dagesh == "dagesh")
                    $englishStr = "ll";
                break;
            case "m":
                $englishStr = "m";
                if ($dagesh == "dagesh")
                    $englishStr = "mm";

                break;
            case "n":
                $englishStr = "n";
                if ($dagesh == "dagesh")
                    $englishStr = "nn";

                break;
            case "o":
                $englishStr = "s";
                if ($dagesh == "dagesh")
                    $englishStr = "ss";

                break;
            case "p":
                /// Only nikud like alef
                if (($prevOtName != "") && ($nextOtName != ""))
                    $englishStr = "'";
                break;
            case "q":
                if ($dagesh != "dagesh")
                    $englishStr = "f";
                else
                    $englishStr = "p";
                break;
            case "r":
                $englishStr = "tz";
                break;
            case "s":
                $englishStr = "k";
                if ($dagesh == "dagesh")
                    $englishStr = "kk";

                break;
            case "t":
                $englishStr = "r";
                break;
            case "u":
                $englishStr = "sh";
                break;
            case "sin":
                $englishStr = "s";
                break;
            case "v":
                $englishStr = "t";
                if ($dagesh == "dagesh")
                    $englishStr = "tt";
                break;

        }


        // In case of duplicate letter like tt because of dagesh -> we need to check that this letter is not first, and not last letter in the word.
        // put duplicate letter , only in the middle of the word.
        if (strlen($englishStr) == 2) {

            if ($englishStr[0] == $englishStr[1]) {
                if (($prevOtName == "") || ($nextOtName == "")) {
                    $englishStr = $englishStr[0];
                }

            }
        }


        //  Shva Na
        if ($nikud == "C" && ($nextOtName != "") && ($otName != "p")) {
            /// Shv-na check
            if ($isFirstLetter)
                $nikud = ":"; // Shva Na Sign
            if ($dagesh == "dagesh")
                $nikud = ":";
            // Domot - הללו  - halelu
            if ($nextOtName == $otName)
                $nikud = ":";

            if (($prevNikud == "C") && ($prevOtName != $otName))
                $nikud = ":";

            if (self::IsTenuaGedola($prevNikud))
                $nikud = ":";

        }

        // Get Nikud

        $englishOtNikud = self::GetNikudLetter($nikud);

        // Patahc Ganuv
        if (($otName == "h") && ($nextOtName == "") && ($nikud == "E"))
            $englishStr = $englishOtNikud . $englishStr;
        else
            $englishStr = $englishStr . $englishOtNikud;

        return $englishStr;

    }


    static    function SplitDoubleNikudWithGersesh($text)
    {
        $nikud1 = "auieo";
        $nikud2 = "auieo";

        $index1 = 0;
        $index2 = 0;

        while ($index1 < 5) {
            while ($index2 < 5) {
                $first = $nikud1[$index1];
                $second = $nikud2[$index2];
                if (($first == "e") && ($second == "i"))
                    continue;
                $text = str_replace($first . $second, $first . "'" . $second, $text);

                $index2++;
            }
            $index1++;
        }

        return $text;

    }

    static function ReplaceShemotHashem($text)
    {
        $firstPart = "Ado";
        $secondPart = "nai";
        $shem = $firstPart . $secondPart;

        $text = str_replace("yehvah", $shem, $text);
        $text = str_replace("yhvh", $shem, $text);
        $text = str_replace("yhvah", $shem, $text);
        $text = str_replace("yehovah", $shem, $text);


        $text = str_replace("yehvh", $shem, $text);

        $firstPart = "elo";
        $secondPart = "him";
        $shem = $firstPart . $secondPart;
        $text = str_replace("yehvih", $shem, $text);

        return $text;

    }


    static function transliterate($text)
    {
        $oneSignText = self::ConvertStr2OneSign($text);
        $lettersArray = self::ParseLetters($oneSignText);
        $englishText = self::ConvertToEnglish($lettersArray);
        $englishText = self::SplitDoubleNikudWithGersesh($englishText);
        $englishText = self::ReplaceShemotHashem($englishText);

        return $englishText;

    }
}