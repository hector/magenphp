<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
/**
 * Stripe component
 */
class StripeComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'api_key' => 'sk_test_QRmYVUl2XJC18d50wJwYTOT3'//test key
    ];

    public function __construct(ComponentRegistry $collection, $settings = array()) {
        parent::__construct($collection, $settings);
        $this->controller = $collection->getController();
        $this->Session = $this->controller->request->getSession();
        $this->_defaultConfig["api_key"] = Configure::read("Stripe.api_key");

        \Stripe\Stripe::setApiKey($this->_defaultConfig["api_key"]);
    }

    function create($data){

        $result = $this->get_token($data["cc"]);

        if($result["result"]){
            try{
                $charge = \Stripe\Charge::create(array(
                    "amount" => ($data["cc"]["amount"] * 100),
                    "currency" => "usd",
                    "source" => $result["token"],
                    "description" => "Magen app candle for (".$data["Candle"]["email"].")"
                ));

                return ["result" => true,"payment" => $charge];

            }catch (\Stripe\Error\Base $e) {
                $this->log("***************************Charge not created", "debug");
                $this->log(print_r($e->getMessage(),true), "debug");
                return ["result" => false,"message" => $e->getMessage()];
            }catch (Exception $e) {
                $this->log("***************************ccharge not created", "debug");
                $this->log(print_r($e->getMessage(),true), "debug");
                return ["result" => false,"message" => $e->getMessage()];
            }
        }else{
            return $result;
        }
    }

    function get_token($card){

        try {
            $token = \Stripe\Token::create(array(
                "card" => array(
                    "number" => $card["number"],
                    "exp_month" => $card["expire_month"],
                    "exp_year" => $card["expire_year"],
                    "cvc" => $card["cvv"]
                )
            ));
            return ["result" => true, "token" => $token];
        }catch (\Stripe\Error\Base $e) {
            $this->log("***************************Token not created", "debug");
            $this->log(print_r($e->getMessage(),true), "debug");
            return ["result" => false,"message" => $e->getMessage()];
        }catch (Exception $e) {
            $this->log("***************************Token not created", "debug");
            $this->log(print_r($e->getMessage(),true), "debug");
            return ["result" => false,"message" => $e->getMessage()];
        }
    }
}
