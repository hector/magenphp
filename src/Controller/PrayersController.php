<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Prayers Controller
 *
 * @property \App\Model\Table\PrayersTable $Prayers
 *
 * @method \App\Model\Entity\Prayer[] paginate($object = null, array $settings = [])
 */
class PrayersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $prayers = $this->paginate($this->Prayers);

        $this->set(compact('prayers'));
        $this->set('_serialize', ['prayers']);
    }

    /**
     * View method
     *
     * @param string|null $id Prayer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $prayer = $this->Prayers->get($id, [
            'contain' => []
        ]);

        $this->set('prayer', $prayer);
        $this->set('_serialize', ['prayer']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $prayer = $this->Prayers->newEntity();
        if ($this->request->is('post')) {
            $prayer = $this->Prayers->patchEntity($prayer, $this->request->getData());
            if ($this->Prayers->save($prayer)) {
                $this->Flash->success(__('The prayer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The prayer could not be saved. Please, try again.'));
        }
        $this->set(compact('prayer'));
        $this->set('_serialize', ['prayer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Prayer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $prayer = $this->Prayers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $prayer = $this->Prayers->patchEntity($prayer, $this->request->getData());
            if ($this->Prayers->save($prayer)) {
                $this->Flash->success(__('The prayer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The prayer could not be saved. Please, try again.'));
        }
        $this->set(compact('prayer'));
        $this->set('_serialize', ['prayer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Prayer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $prayer = $this->Prayers->get($id);
        if ($this->Prayers->delete($prayer)) {
            $this->Flash->success(__('The prayer has been deleted.'));
        } else {
            $this->Flash->error(__('The prayer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
