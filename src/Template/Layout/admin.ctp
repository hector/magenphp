<?php
/**
 * @var \App\View\AppView $this
 */
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= $this->fetch('title') ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <?php echo $this->Html->css([
        'admin/bootstrap.min',
        'admin/font-awesome.min',
        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
        'admin/AdminLTE',
        'admin/_all-skins.min',
        'admin/default.min',
    ]);
    ?>
    <?= $this->fetch('css') ?>
</head>

<body class="sidebar-mini skin-black fixed">

<div class="wrapper">

    <?=$this->element("Admin/header")?>
    <?=$this->element("Admin/sidebar")?>
    <div class="content-wrapper">
        <div>
            <?=$this->Flash->render();?>
        </div>
        <?= $this->fetch('content') ?>
    </div>

</div>
<!-- ./wrapper -->
<?php echo $this->Html->script(['admin/jquery.min','admin/bootstrap.min','admin/jquery.slimscroll.min','admin/app.min','admin/adminlte.min']);?>
<?=$this->App->js(); ?>
<?= $this->fetch('script') ?>
<?= $this->fetch('scriptBottom') ?>
</body>

</html>