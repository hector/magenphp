<?php
/**
 * @var \App\View\AppView $this
 */
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= $this->fetch('title') ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <?php echo $this->Html->css([
        'admin/bootstrap.min',
        'admin/AdminLTE'
    ]);
    ?>
</head>

<body class="login-page">


<div class="login-box">
    <div class="login-logo">
        <a href="<?=$this->Url->build("/",true)?>"><?=__("Magen");?></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">
            Sign in to start your session
            <div class="text-center">
                <?php echo $this->Flash->render();
                echo $this->Flash->render('auth'); ?>
            </div>
        </p>

        <?php echo $this->Form->create('User',['id' =>'login-form']) ?>
            <div class="form-group has-feedback">
                <?php echo $this->Form->control('email',array('class'=>'form-control','placeholder'=>__("Email"),'required'));?>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?php echo $this->Form->input('password',array('class'=>'form-control','placeholder'=>__("Password"),'required'));?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn bg-navy btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        <div class="row">
            <div class="col-xs-12">
                    <?php // echo $this->Html->link(__('Back to site'), ['controller' =>'Posts','action' => 'home','_full' => true,'prefix' => false]); ?>
            </div>
        </div>
        <?php $this->Form->end(); ?>

    </div>
</div>
<?php echo $this->Html->script(['admin/jquery.min','admin/bootstrap.min','admin/app.min','admin/adminlte.min']);?>
</body>

</html>