<?php
/**
 * @var \App\View\AppView $this
 */
?>
<header class="main-header">
    <a href="<?=$this->Url->build("/admin")?>" class="logo">
        <span class="logo-mini"><b>M</b></span>
        <span class="logo-lg"><b>Magen</b></span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="<?=$this->Url->build(["controller" => 'Users','action' => 'profile'])?>">
                         <span class="hidden-xs"><?=$this->request->getSession()->read("Auth.User.name")?></span>
                    </a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="<?=$this->Url->build(["controller" => 'Users','action' => 'logout'])?>">
                        <?=__("Logout");?>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
