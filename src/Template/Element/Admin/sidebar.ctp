<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search..." value="<?=$this->request->getQuery("search")?>">
                <span class="input-group-btn">
                    <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <ul class="sidebar-menu tree" data-animation-speed="250">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="<?=$this->Url->build(["controller" => 'Pages','action' =>'index'])?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>

                </a>

            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i> <span><?=__("Prayers");?></span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?=$this->Url->build(["controller" => 'Prayers','action' =>'index'])?>"><i class="fa fa-circle-o"></i> <?=__("All prayers");?></a></li>
                    <li><a href="<?=$this->Url->build(["controller" => 'Prayers','action' =>'add'])?>"><i class="fa fa-circle-o"></i> <?=__("Add new");?></a></li>
                    <li><a href="<?=$this->Url->build(["controller" => 'Prayers','action' =>'order'])?>"><i class="fa fa-circle-o"></i> <?=__("Order");?></a></li>
                    <li><a href="<?=$this->Url->build(["controller" => 'Categories','action' =>'index'])?>"><i class="fa fa-circle-o"></i> <?=__("Categories");?></a></li>
                    <li><a href="<?=$this->Url->build(["controller" => 'Categories','action' =>'order'])?>"><i class="fa fa-circle-o"></i> <?=__("Categories order");?></a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i> <span><?=__("Psalms");?></span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?=$this->Url->build(["controller" => 'Psalms','action' =>'index'])?>"><i class="fa fa-circle-o"></i> <?=__("All psalms");?></a></li>
                    <li><a href="<?=$this->Url->build(["controller" => 'Prayers','action' =>'add'])?>"><i class="fa fa-circle-o"></i> <?=__("Add new")  ;?></a></li>
                </ul>
            </li>
            <li >
                <a href="<?=$this->Url->build(["controller" => 'Candles','action' =>'index'])?>">
                    <i class="fa fa-fire"></i> <span><?=__("Candles");?></span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-font"></i> <span><?=__("Texts");?></span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?=$this->Url->build(["controller" => 'Texts','action' =>'index'])?>"><i class="fa fa-circle-o"></i> <?=__("All texts");?></a></li>
                    <li><a href="<?=$this->Url->build(["controller" => 'Texts','action' =>'add'])?>"><i class="fa fa-circle-o"></i> <?=__("Add new");?></a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-money"></i> <span><?=__("Payments");?></span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?=$this->Url->build(["controller" => 'PaymentsValues','action' =>'index'])?>"><i class="fa fa-circle-o"></i> <?=__("Payment values");?></a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span><?=__("Users");?></span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?=$this->Url->build(["controller" => 'Users','action' =>'index'])?>"><i class="fa fa-circle-o"></i> <?=__("All Users");?></a></li>
                    <li><a href="<?=$this->Url->build(["controller" => 'Users','action' =>'add'])?>"><i class="fa fa-circle-o"></i> <?=__("Add new");?></a></li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>