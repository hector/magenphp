<section class="content-header">
    <h1>
        <?=__("Users");?>
        <small><?=__("Edit");?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>

    </ol>
</section>
<section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo $this->Form->create($user, array(
                'class' => '',
                'role' => 'form',
                'type' => 'file',
                'novalidate' => true,

            )); ?>
            <div class="form-group">
                <label>Email</label>
                <?php echo $this->Form->control('Users.email',array("type"=>"text",'class' => 'form-control','label' => false,'disabled'=>'disabled'));?>
            </div>
            <div class="form-group">
                <label>Name</label>
                <?php echo $this->Form->control('Users.name',array("type"=>"text",'class' => 'form-control','label' => false));?>
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <?php echo $this->Form->control('Users.lastname',array("type"=>"text",'class' => 'form-control','label' => false));?>
            </div>
<!--            <div class="form-group">
                <label>Password</label>
                <?php /*echo $this->Form->control('Users.password', array("type" => "password", 'class' => 'form-control underlined', 'label' => false)); */?>
            </div>-->
            <div class="form-group">
                <label>Active</label>
                <label>
                    <?php echo $this->Form->control('Users.active', array("type" => "checkbox", 'class' => 'checkbox', 'label' => false)); ?>
                    <span></span>
                </label>
            </div>
            <div class="form-group">
                <label>Role</label>
                <?php echo $this->Form->control('Users.role',array("options"=>['editor'=>'Editor','admin'=>'Admin'],'class' => 'form-control','label' => false));?>
            </div>
            <div class="form-group text-right">
                <button class="btn bg-gray-active btn-flat"><i class="fa fa-edit"></i> <?=__("Save");?></button>
            </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>
</section>