<section class="content-header">
    <h1>
        <?=__("Admin users");?>
        <small><?=date("D/m/Y")?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>

    </ol>
</section>

<section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-striped table-hover flip-content">
                        <thead class="flip-header">
                        <tr>
                            <th><?php echo $this->Paginator->sort('id', ' ID',array('class'=>'icon-sort'));?></th>
                            <th><?php echo $this->Paginator->sort('name', ' Name',array('class'=>'icon-sort'));?></th>
                            <th><?php echo $this->Paginator->sort('lastname', ' Lastname',array('class'=>'icon-sort'));?></th>
                            <th><?php echo $this->Paginator->sort('role', ' Role',array('class'=>'icon-sort'));?></th>
                            <th><?php echo $this->Paginator->sort('active', ' Status',array('class'=>'icon-sort'));?></th>
                            <th><?php echo __("Action")?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($rows as $key => $row) {?>
                            <tr>
                                <td><?php echo $row->id ?></td>
                                <td><?php echo $row->name ?></td>
                                <td><?php echo $row->lastname ?></td>
                                <td><?php echo $row->role?></td>

                                <td>
                                    <?php
                                    $class = ($row->active==1)?'label-success':'label-danger';
                                    $type = ($row->active==1)?'Active':'Inactive';
                                    ?>
                                    <label class="label <?php echo $class ?>"><?php echo $type ?></label>

                                </td>
                                <td>
                                    <?php echo $this->Html->link(
                                        '<i class="fa-edit fa fa-1"></i>',
                                        array(
                                            'controller' => 'users',
                                            'action' => 'edit',
                                            $row->id,
                                        ),array('escape' => false,'class'=>'btn btn-xs btn-info btn-flat','title'=>__("Edit"))
                                    );?>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="navigation" class="text-center">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('<') ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next('>') ?>
                        </ul>
                    </nav>
                    <p class="text-center"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    <p class="text-right">
                        <?php echo $this->Html->link(
                            '<i class="fa fa-user"></i> '.__("New user"),
                            array(
                                'controller' => 'users',
                                'action' => 'add',
                            ),array('escape' => false,'class'=>'btn bg-gray-active btn-flat')
                        );?>
                    </p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>