<section class="content-header">
    <h1>
        <?=__("Users");?>
        <small><?=__("Change password");?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> <?=__("Home");?></a></li>

    </ol>
</section>
<section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo $this->Form->create($user, array(
                'class' => '',
                'role' => 'form',
                'type' => 'file',
                'novalidate' => true,

            )); ?>
            <div class="form-group">
                <label><?=__("Current Password");?></label>
                <?php echo $this->Form->control('old_password',array('type'=>'password','class'=>'form-control','placeholder'=>__("Your old password"),'value' => ''));?>
            </div>
            <div class="form-group">
                <label><?=__("New password");?></label>
                <?php echo $this->Form->control('password1',array('type'=>'password','class'=>'form-control','placeholder'=>__("New password"),'value' => ''));?>
            </div>
            <div class="form-group">
                <label><?=__("Repeat password");?></label>
                <?php echo $this->Form->control('password2',array('type'=>'password','class'=>'form-control','placeholder'=>__("New password"),'value' => ''));?>
            </div>
            <div class="form-group text-right">
                <button class="btn bg-gray-active btn-flat"><i class="fa fa-edit"></i> <?=__("Update password");?></button>
            </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>
</section>