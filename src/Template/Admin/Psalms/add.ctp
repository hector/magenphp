<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Psalms'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="psalms form large-9 medium-8 columns content">
    <?= $this->Form->create($psalm) ?>
    <fieldset>
        <legend><?= __('Add Psalm') ?></legend>
        <?php
            echo $this->Form->control('hebrew_name');
            echo $this->Form->control('name');
            echo $this->Form->control('chapter');
            echo $this->Form->control('verse_number');
            echo $this->Form->control('verse_number_hebrew');
            echo $this->Form->control('verse_text_hebrew');
            echo $this->Form->control('verse_text');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
