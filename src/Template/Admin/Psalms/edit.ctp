<?php
/**
 * @var \App\View\AppView $this
 */

echo $this->Html->script([
    '//cdn.ckeditor.com/4.6.2/full/ckeditor.js',
]);

?>
<section class="content-header">
    <h1>
        <?=__("Psalms");?>
        <small><?=__("editing a psalm")?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>

    </ol>
</section>

<section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo $this->Form->create($psalm, array(
                'novalidate' => true,
                'class' => '',
                'role' => 'form',
                'type' => 'file'
            )); ?>

            <div class="form-group">
                <label><?=__("Psalm");?></label>
                <?php echo $this->Form->control('Psalms.name', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label><?=__("Description");?></label>
                <?php echo $this->Form->control('Psalms.description', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label><?=__("Text");?></label></small>
                <?php echo $this->Form->control('Psalms.prayer', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label><?=__("Psalm explanation");?></label>
                <?php echo $this->Form->control('Psalms.explanation', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label><?=__("Mp3 file");?></label>
                <?php echo $this->Form->control('Psalms.mp3', ['type' => 'file','class' => 'form-control']); ?>
                <?php if($psalm->get("mp3")){?>
                    <audio controls preload="metadata">
                        <source src="<?=$this->Url->build("/",true).str_replace("webroot","",$psalm->get("dir").$psalm->get("mp3"))?>" type="audio/mpeg">
                        Your browser does not support the audio element.
                    </audio>
                <?php } ?>
            </div>

            <div class="form-group text-right">
                <button class="btn bg-gray-active btn-flat"><i class="fa fa-edit"></i> <?=__("Save");?></button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php $this->Html->scriptStart(['block' => true]);?>
    <!--<script>-->
    $(function(){
    CKEDITOR.replace("psalms-description");
    CKEDITOR.replace("psalms-prayer");
    CKEDITOR.replace("psalms-explanation");
    CKEDITOR.config.stylesSet = [
    { name: 'Hebrew text', element: 'span', attributes: { 'class': 'zohar-text-no-bg' } }
    ];
    });
    CKEDITOR.config.filebrowserUploadUrl = Admin.basePath+'admin/pages/upload_editor';
    //CKEDITOR.config.extraPlugins = 'uploadimage';
    CKEDITOR.config.uploadUrl = Admin.basePath+'admin/pages/upload_editor?met=drop';
    CKEDITOR.config.allowedContent = true;
    <!--</script>-->

<?php echo $this->Html->scriptEnd();?>