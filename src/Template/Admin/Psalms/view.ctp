<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Psalm $psalm
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Psalm'), ['action' => 'edit', $psalm->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Psalm'), ['action' => 'delete', $psalm->id], ['confirm' => __('Are you sure you want to delete # {0}?', $psalm->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Psalms'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Psalm'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="psalms view large-9 medium-8 columns content">
    <h3><?= h($psalm->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Hebrew Name') ?></th>
            <td><?= h($psalm->hebrew_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($psalm->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Verse Number Hebrew') ?></th>
            <td><?= h($psalm->verse_number_hebrew) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($psalm->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Chapter') ?></th>
            <td><?= $this->Number->format($psalm->chapter) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Verse Number') ?></th>
            <td><?= $this->Number->format($psalm->verse_number) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Verse Text Hebrew') ?></h4>
        <?= $this->Text->autoParagraph(h($psalm->verse_text_hebrew)); ?>
    </div>
    <div class="row">
        <h4><?= __('Verse Text') ?></h4>
        <?= $this->Text->autoParagraph(h($psalm->verse_text)); ?>
    </div>
</div>
