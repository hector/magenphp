<section class="content-header">
    <h1>
        Home
        <small><?=date("D/m/Y")?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>

    </ol>
</section>

<section class="content">


    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Statistics</h3>

        </div>
        <div class="box-body">
            Statistics page
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>