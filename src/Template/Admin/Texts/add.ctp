<?php
/**
 * @var \App\View\AppView $this
 */

echo $this->Html->script([
    '//cdn.ckeditor.com/4.6.2/full/ckeditor.js',
]);

?>
<section class="content-header">
    <h1>
        <?=__("Texts");?>
        <small><?=__("Add a new text")?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>

    </ol>
</section>

<section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo $this->Form->create($text, array(
                'novalidate' => true,
                'class' => '',
                'role' => 'form',
                'type' => 'file'
            )); ?>

            <div class="form-group">
                <label><?=__("Title");?></label>
                <?php echo $this->Form->control('Texts.title', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label><?=__("Description");?></label>
                <?php echo $this->Form->control('Texts.description', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label><?=__("Abbreviation");?></label> <small class="text-gray text-italic"><?= __("must be unique");?></small>
                <?php echo $this->Form->control('Texts.abbrev', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group text-right">
                <button class="btn bg-gray-active btn-flat"><i class="fa fa-edit"></i> <?=__("Add");?></button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php $this->Html->scriptStart(['block' => true]);?>
<!--<script>-->
$(function(){
CKEDITOR.replace("texts-description");
CKEDITOR.config.stylesSet = [
{ name: 'Hebrew text', element: 'span', attributes: { 'class': 'zohar-text-no-bg' } }
];
});
<!--</script>-->

<?php echo $this->Html->scriptEnd();?>
