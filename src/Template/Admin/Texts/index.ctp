<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Prayer[]|\Cake\Collection\CollectionInterface $prayers
 */
?>
<section class="content-header">
    <h1>
        <?=__("App texts");?>
        <small><?=__("all texts");?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('abbrev') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($texts as $text): ?>
                    <tr>
                        <td><?= $this->Number->format($text->id) ?></td>
                        <td><?= h($text->title) ?></td>
                        <td><?= h($text->abbrev) ?></td>
                        <td class="actions">
                            <?php echo $this->Html->link(
                                '<i class="fa-edit fa fa-1"></i>',
                                array(
                                    'action' => 'edit',
                                    $text->id,
                                ),array('escape' => false,'class'=>'btn btn-xs btn-info btn-flat','title'=>__("Edit"))
                            );?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <nav aria-label="navigation" class="text-center">
                    <ul class="pagination">
                        <?= $this->Paginator->prev('<') ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next('>') ?>
                    </ul>
                </nav>
                <p class="text-center"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                <p class="text-right">
                    <?php echo $this->Html->link(
                        '<i class="fa fa-list"></i> '.__("New text"),
                        array(
                            'action' => 'add',
                        ),array('escape' => false,'class'=>'btn bg-gray-active btn-flat')
                    );?>
                </p>
            </div>
        </div>
    </div>
</section>