<?php
/**
 * @var \App\View\AppView $this
 */
?>
<section class="content-header">
    <h1>
        <?=__("Candles");?>
        <small><?=__("Edit this candle")?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>

    </ol>
</section>

<section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo $this->Form->create($candle, array(
                'novalidate' => true,
                'class' => '',
                'role' => 'form',
                'type' => 'file'
            )); ?>
            <div class="form-group">
                <label><?=__("Email");?></label>
                <?php echo $this->Form->control('Candles.email', ['class' => 'form-control','disabled']); ?>
            </div>
            <div class="form-group">
                <label><?=__("Name");?></label>
                <?php echo $this->Form->control('Candles.name', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label><?=__("Parent name");?></label>
                <?php echo $this->Form->control('Candles.parent_name', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label><?=__("Prayer");?></label>
                <?php echo $this->Form->select(
                    'Candles.prayer_id',
                    $prayers,
                    ["class" =>"form-control"]

                ); ?>
            </div>
            <div class="form-group">
                <label><?=__("Type");?></label>
                <?php echo $this->Form->select(
                    'Candles.donation',
                    $donations,
                    ["class" =>"form-control"]

                ); ?>
            </div>
            <div class="form-group">
                <label><?=__("Gender");?></label>
                <?php echo $this->Form->select(
                    'Candles.gender',
                    ["ben" => "Male","bat" => "Female"],
                    ["class" =>"form-control"]

                ); ?>
            </div>
            <div class="form-group text-right">
                <button class="btn bg-gray-active btn-flat"><i class="fa fa-edit"></i> <?=__("Edit");?></button>
            </div>
            <?= $this->Form->end() ?>

        </div>
    </div>
</section>
