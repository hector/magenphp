<?php
/**
 * @var \App\View\AppView $this
 */
?>
<section class="content-header">
    <h1>
        <?=__("Categories");?>
        <small><?=__("Edit this category")?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>

    </ol>
</section>

<section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo $this->Form->create($category, array(
                'novalidate' => true,
                'class' => '',
                'role' => 'form',
                'type' => 'file'
            )); ?>

            <div class="form-group">
                <label><?=__("Title");?></label>
                <?php echo $this->Form->control('Categories.title', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label><?=__("Description");?></label>
                <?php echo $this->Form->control('Categories.description', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group text-right">
                <button class="btn bg-gray-active btn-flat"><i class="fa fa-edit"></i> <?=__("Add");?></button>
            </div>
            <?= $this->Form->end() ?>

        </div>
    </div>
</section>
