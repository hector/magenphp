<?php
/**
 * @var \App\View\AppView $this
 */

echo $this->Html->script([
    '//cdnjs.cloudflare.com/ajax/libs/nestable2/1.5.0/jquery.nestable.min.js',
    'admin/jquery.bootstrap-growl.min',
    'admin/categories.min',
],
    ['block' => 'scriptBottom']
);
echo $this->Html->css([
    '//cdnjs.cloudflare.com/ajax/libs/nestable2/1.5.0/jquery.nestable.min.css',
],['block'=>'css']
);


?>
<section class="content-header">
    <h1>
        <?=__("Categories");?>
        <small><?=__("set order to the categories list");?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid">
        <p class="pad"><?=__("Drag the category to the desire position");?></p>
        <div class="box-body">
            <div class="row">
                <div class="col-md-8 col-md-push-2">
                    <div class="dd prayers-order-list">
                        <ol class="dd-list">
                            <?php foreach ($categories as $category) {?>
                                <li class="dd-item" data-id="<?=$category->id?>">
                                    <div class="dd-handle"><?=$category->get("title")?></div>
                                </li>
                            <?php }?>
                        </ol>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>