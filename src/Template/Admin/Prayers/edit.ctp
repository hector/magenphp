<?php
/**
 * @var \App\View\AppView $this
 */

echo $this->Html->script([
    '//cdn.ckeditor.com/4.6.2/full/ckeditor.js',
    '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js',
    'admin/prayers.min',
],
    ['block' => 'scriptBottom']
);
echo $this->Html->css([
    '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css',
],['block'=>'css']
);


?>
<section class="content-header">
    <h1>
        <?=__("Prayers");?>
        <small><?=__("editing prayer")?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>

    </ol>
</section>

<section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo $this->Form->create($prayer, array(
                'novalidate' => true,
                'class' => '',
                'role' => 'form',
                'type' => 'file'
            )); ?>
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label><?=__("Title");?></label>
                        <?php echo $this->Form->control('Prayers.title', ['class' => 'form-control']); ?>
                    </div>
                    <div class="form-group">
                        <label><?=__("Description");?></label>
                        <?php echo $this->Form->control('Prayers.description', ['class' => 'form-control']); ?>
                    </div>
                    <div class="form-group">
                        <label><?=__("Prayer text");?></label>
                        <?php echo $this->Form->control('Prayers.text', ['class' => 'form-control']); ?>
                    </div>
                    <div class="form-group">
                        <label><?=__("Prayer transliteration");?></label>
                        <?php echo $this->Form->control('Prayers.transliteration', ['class' => 'form-control']); ?>
                    </div>

                    <div class="form-group">
                        <label><?=__("Prayer explanation");?></label>
                        <?php echo $this->Form->control('Prayers.explanation', ['class' => 'form-control']); ?>
                    </div>

                    <!--<div class="form-group">
                        <label><?/*=__("Type");*/?></label>
                        <?php /*echo $this->Form->select('Prayers.type',$types ,['class' => 'form-control','id'=>'prayers-type']); */?>
                    </div>-->
                    <div class="form-group">
                        <label><?=__("Mp3 file");?></label>
                        <?php echo $this->Form->control('Prayers.mp3', ['type' => 'file','class' => 'form-control']); ?>
                        <?php if($prayer->get("mp3")){?>
                        <audio controls preload="metadata">
                            <source src="<?=$this->Url->build("/",true).str_replace("webroot","",$prayer->get("dir").$prayer->get("mp3"))?>" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label><?=__("Order");?></label>
                        <?php echo $this->Form->control('Prayers.position', ['class' => 'form-control']); ?>
                    </div>
                    <div class="form-group">
                        <label><?=__("Categories");?></label>
                        <?php echo $this->Form->select('Prayers.categories._ids',
                            $categories,
                            ['multiple' => true,'class' =>'form-control select-2','style'=>'width:100%']); ?>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label> <strong><?php echo $this->Form->checkbox('Prayers.paid', ['class' => '']); ?> <?=__("Is this a paid prayer?");?></strong></label>
                        </div>
                        <div class="checkbox">
                            <label> <strong><?php echo $this->Form->checkbox('Prayers.active', ['class' => '']); ?> <?=__("Active");?></strong></label>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button class="btn bg-gray-active btn-block btn-flat"><i class="fa fa-edit"></i> <?=__("Update");?></button>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>

        </div>
    </div>
</section>
