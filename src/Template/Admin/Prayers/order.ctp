<?php
/**
 * @var \App\View\AppView $this
 */

echo $this->Html->script([
    'admin/Sortable.min',
    'admin/jquery.bootstrap-growl.min',
    'admin/prayers.min',
],
    ['block' => 'scriptBottom']
);
echo $this->Html->css([
    '//cdnjs.cloudflare.com/ajax/libs/nestable2/1.5.0/jquery.nestable.min.css',
],['block'=>'css']
);

//pr($categories);
?>
<section class="content-header">
    <h1>
        <?=__("Prayers");?>
        <small><?=__("set order to the prayers list");?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid">
        <p class="pad"><?=__("Drag the prayer to the desire position");?></p>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-2">
                    <h3 class="margin-bottom">Categories</h3>
                    <hr>
                    <div class="dd categories-list">
                        <?php foreach ($categories as $category) {?>
                        <h4>- <?=$category->get("title")?></h4>
                        <ol class="dd-list" id="category-<?=$category->id?>" style="min-height: 20px" data-category-id="<?=$category->id?>">
                            <?php foreach ($category->prayers as $p) {?>
                                <li class="dd-item" data-id="<?=$p->id?>" data-order="<?=$p->get("_joinData")->position?>">
                                    <div class="dd-handle"><?=$p->get("title")?>
                                    </div>
                                </li>
                           <?php  }?>
                        </ol>
                        <hr>
                        <?php }?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>