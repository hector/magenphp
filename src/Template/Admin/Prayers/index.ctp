<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Prayer[]|\Cake\Collection\CollectionInterface $prayers
  */
?>
<section class="content-header">
    <h1>
        <?=__("Prayers");?>
        <small><?=__("all prayers");?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                        <th class="text-center"  scope="col"><?= $this->Paginator->sort('position') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('paid') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('active', "Status") ?></th>
                        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($prayers as $prayer): ?>
                    <tr>
                        <td><?= $this->Number->format($prayer->id) ?></td>
                        <td><?= h($prayer->title) ?></td>
                        <td class="text-center"><?= $this->Number->format($prayer->position) ?></td>
                        <td>
                            <?php if($prayer->paid){?>
                                <span data-toggle="tooltip" title="" class="badge bg-green-active"><?=__("Yes");?></span>
                            <?php }else{ ?>
                                <span data-toggle="tooltip" title="" class="badge bg-red"><?=__("No");?></span>
                            <?php } ?>
                        </td>
                        <td><?= h($prayer->type) ?></td>
                        <td>
                            <?php if($prayer->active){?>
                                <span data-toggle="tooltip" title="" class="badge bg-green-active"><?=__("Active");?></span>
                            <?php }else{ ?>
                                <span data-toggle="tooltip" title="" class="badge bg-red"><?=__("Inactive");?></span>
                            <?php } ?>
                        </td>
                        <td><?= h($prayer->modified) ?></td>
                        <td class="actions">
                            <?php echo $this->Html->link(
                                '<i class="fa-edit fa fa-1"></i>',
                                array(
                                    'action' => 'edit',
                                    $prayer->id,
                                ),array('escape' => false,'class'=>'btn btn-xs btn-info btn-flat','title'=>__("Edit"))
                            );?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <nav aria-label="navigation" class="text-center">
                    <ul class="pagination">
                        <?= $this->Paginator->prev('<') ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next('>') ?>
                    </ul>
                </nav>
                <p class="text-center"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                <p class="text-right">
                    <?php echo $this->Html->link(
                        '<i class="fa fa-list"></i> '.__("New prayer"),
                        array(
                            'controller' => 'prayers',
                            'action' => 'add',
                        ),array('escape' => false,'class'=>'btn bg-gray-active btn-flat')
                    );?>
                </p>
            </div>
        </div>
    </div>
</section>