<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header">
    <h1>
        <?=__("Payments values");?>
        <small><?=__("editing a value")?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->Url->build(["controller"=>'Pages','action' =>'index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>

    </ol>
</section>
<section class="content">
    <div class="box box-solid">
        <div class="box-body">
        <?php echo $this->Form->create("PaymentsValues", array(
            'class' => 'form-horizontal',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true
        )); ?>
        <div class="row">
            <div class="col-md-5 center-block col-md-push-3">
                <div class="panel panel-default">
                    <div class="panel-body hidden"></div>
                    <table class="table">
                        <thead>
                        <th>#</th>
                        <th>Days</th>
                        <th>Price</th>
                        </thead>
                        <tbody>
                        <?php foreach ($paymentsValues as $key => $pay) {?>
                            <tr>
                                <td class="text-center"><?= $key +1?></td>
                                <td>
                                    <?=$this->Form->control('PaymentsValues.'.$key.'.id',['type'=>'hidden','value'=>$pay->id]);
                                    ?>
                                    <div class="input-group">
                                        <?php echo $this->Form->control('PaymentsValues.'.$key.'.days',
                                           ['class' => 'form-control','type'=>'text','placeholder'=>'days','value'=>$pay->days]);

                                        ?>
                                        <span class="input-group-addon">days</span>

                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <?php echo $this->Form->control('PaymentsValues.'.$key.'.price',
                                            array('class' => 'form-control price-box','type'=>'text','placeholder'=>'price','value'=>$pay->price));
                                        ?>
                                    </div>


                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'),array('class'=>'btn btn-primary'));?>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</div>
</section>