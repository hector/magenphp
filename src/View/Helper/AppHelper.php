<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Filesystem\File;
class AppHelper extends Helper
{
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Url',
        'Js',
    );

    public function js() {
        $shop = array();
        $shop['basePath'] = $this->Url->build('/',true);


        $shop['params'] = array(
            'controller' => $this->request->getParam('controller'),
            'action' => $this->request->getParam('action'),
            'prefix' => $this->request->getParam('prefix'),
        );
        return $this->Html->scriptBlock('var Admin = ' . json_encode($shop) . ';');
    }
    public function front() {
        $front = array();
        $front['basePath'] = $this->Url->build('/',true);

        $lang = $this->request->getParam('lang')?$this->request->getParam('lang'):"en";

        $front['params'] = array(
            'controller' => $this->request->getParam('controller'),
            'action' => $this->request->getParam('action'),
            'lang' => $lang,
        );
        return $this->Html->scriptBlock('var _front = ' . json_encode($front) . ';');
    }

    function addOrdinalNumberSuffix($num) {
        if (!in_array(($num % 100),array(11,12,13))){
            switch ($num % 10) {
                // Handle 1st, 2nd, 3rd
                case 1:  return $num.'st';
                case 2:  return $num.'nd';
                case 3:  return $num.'rd';
            }
        }
        return $num.'th';
    }
}
