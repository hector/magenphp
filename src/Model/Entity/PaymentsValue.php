<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PaymentsValue Entity
 *
 * @property int $id
 * @property int $days
 * @property float $price
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class PaymentsValue extends Entity
{


    protected function _getLabel()
    {
        return $this->_properties['days'] . ' days';

    }
}
