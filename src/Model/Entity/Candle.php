<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Candle Entity
 *
 * @property int $id
 * @property string $name
 * @property string $parent_name
 * @property string $email
 * @property int $donation
 * @property int $payment_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Payment $payment
 */
class Candle extends Entity
{
    protected $_virtual = ["hebrew","gender_hebrew"];
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected function _getHebrew(){
        if($this->isNew()){
            return;
        }
        if(preg_match("/\p{Hebrew}/u", $this->_properties["name"])){
            return true;
        }
        return false;
    }

    protected function _getGenderHebrew(){

        if(preg_match("/\p{Hebrew}/u", $this->_properties["name"])) {
            return ($this->_properties["gender"] == "ben") ? "בן" : "בת";
        }
        return false;
    }

}
