<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CategoriesPrayer Entity
 *
 * @property int $id
 * @property int $category_id
 * @property int $prayer_id
 * @property int $position
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Prayer $prayer
 */
class CategoriesPrayer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
