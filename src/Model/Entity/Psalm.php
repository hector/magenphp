<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Routing\Router;
/**
 * Psalm Entity
 *
 * @property int $id
 * @property string $hebrew_name
 * @property string $name
 * @property int $chapter
 * @property int $verse_number
 * @property string $verse_number_hebrew
 * @property string $verse_text_hebrew
 * @property string $verse_text
 */
class Psalm extends Entity
{

    protected $_virtual = ["audio_link"];
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected function _getAudioLink()
    {
        if($this->get("mp3")){
            return Router::url("/",true).str_replace("webroot","",$this->get("dir").$this->get("mp3"));
        }
        return null;

    }
}
