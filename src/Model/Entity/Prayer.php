<?php
namespace App\Model\Entity;

use App\Controller\Component\TransliterateComponent;
use Cake\ORM\Entity;
use Cake\Routing\Router;
/**
 * Prayer Entity
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $text
 * @property string $transliteration
 * @property int $order
 * @property bool $paid
 * @property string $type
 * @property string $link
 * @property bool $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $slug
 */
class Prayer extends Entity
{
    protected $_virtual = ["audio_link","formatted_text"];


    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /*protected function _getTransliteration()
    {

        if(preg_match("/\p{Hebrew}/u", $this->_properties["text"])){
            $text = preg_replace('/<[^<]+?>/', ' ', $this->_properties['text']);
            return TransliterateComponent::transliterate($text);
        }
        return null;

    }*/
    protected function _getAudioLink()
    {
        if($this->get("mp3")){
            return Router::url("/",true).str_replace("webroot/","",$this->get("dir").$this->get("mp3"));
        }
        return null;

    }

    protected function _getFormattedText(){
        $text = $this->_properties["text"];
        $patron = '/(?:https?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?([\w\-]{10,12})(?:&feature=related)?(?:[\w\-]{0})?/';
        $sustitucion = '<div class="video-container embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/$1?controls=2" allowfullscreen></iframe></div>';
        $text = preg_replace($patron, $sustitucion,$text);
        //vimeo
        $patron = '/(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(\d+)/';
        $sustitucion = '<div class=" video-container embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/$1" allowfullscreen></iframe></div>';
        $text = preg_replace($patron, $sustitucion, $text);
        return $text;

    }
}
