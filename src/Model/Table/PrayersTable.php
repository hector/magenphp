<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\ORM\Entity;
use Cake\Utility\Text;
/**
 * Prayers Model
 *
 * @method \App\Model\Entity\Prayer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Prayer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Prayer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Prayer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Prayer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Prayer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Prayer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PrayersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('prayers');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Sluggable', [
            'implementedFinders' => [
                'slugged' => 'findSlug',
            ]
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'mp3' => []
        ]);

        $this->hasMany('Candles', [
            'foreignKey' => 'prayer_id'
        ]);

        $this->belongsToMany('Categories', [
            'through' => 'CategoriesPrayers',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->scalar('text')
            ->requirePresence('text', 'create')
            ->notEmpty('text');


        $validator
            ->boolean('paid')
            ->requirePresence('paid', 'create')
            ->notEmpty('paid');

        $validator
            ->add('mp3', [
                'uploadError' => [
                    'rule' => 'uploadError',
                    'message' => 'The mp3 upload failed.',
                    'allowEmpty' => true,
                ],

                'mimeType' => [
                    'rule' => array('mimeType', array('audio/mp3',"audio/mpeg",'audio/x-mpeg','audio/x-mp3','audio/mpeg3','audio/x-mpeg3','audio/mpg','audio/x-mpg','audio/x-mpegaudio')),
                    'message' => 'Please only upload mp3 files',
                    'allowEmpty' => true,
                ],

            ]);
        $validator->allowEmpty('mp3');
        $validator->allowEmpty('dir');


        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {

        if (isset($data['image']) && $data["image"]) {
            $data['image']["name"] = Text::slug($data['image']["name"],['preserve' => '.']);
        }

    }


    function getTypes(){
        return [
            'text' => 'Text',
            'mp3' => 'Mp3 file',
            'imagelink' => 'Image link',
            'vimeo' => 'Vimeo video',
            'youtube' => 'Youtube video',
            'pdf' => 'Pdf file link',
        ];
    }
}
