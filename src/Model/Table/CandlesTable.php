<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * Candles Model
 *
 * @property \App\Model\Table\PaymentsTable|\Cake\ORM\Association\BelongsTo $Payments
 *
 * @method \App\Model\Entity\Candle get($primaryKey, $options = [])
 * @method \App\Model\Entity\Candle newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Candle[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Candle|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Candle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Candle[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Candle findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CandlesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('candles');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Payments', [
            'foreignKey' => 'payment_id'
        ]);
        $this->belongsTo('Prayers', [
            'foreignKey' => 'prayer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->allowEmpty('name');

        $validator
            ->scalar('parent_name')
            ->allowEmpty('parent_name');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->integer('donation')
            ->allowEmpty('donation');

        return $validator;
    }


    public function findByPrayer(Query $query, array $options)
    {
        $prayer_id = $options['prayer_id'];

        return $query->where(['Candles.prayer_id' => $prayer_id,
                '(date(Candles.created) + INTERVAL Candles.donation DAY) >= current_date',
        ]);
    }
}
