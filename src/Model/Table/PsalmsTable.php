<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Psalms Model
 *
 * @method \App\Model\Entity\Psalm get($primaryKey, $options = [])
 * @method \App\Model\Entity\Psalm newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Psalm[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Psalm|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Psalm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Psalm[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Psalm findOrCreate($search, callable $callback = null, $options = [])
 */
class PsalmsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('psalms');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Josegonzalez/Upload.Upload', [

            'mp3' => []
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('hebrew_name')
            ->allowEmpty('hebrew_name');

        $validator
            ->scalar('name')
            ->allowEmpty('name');

        $validator
            ->integer('chapter')
            ->allowEmpty('chapter');

        $validator
            ->integer('verse_number')
            ->allowEmpty('verse_number');

        $validator
            ->scalar('verse_number_hebrew')
            ->allowEmpty('verse_number_hebrew');

        $validator
            ->scalar('verse_text_hebrew')
            ->allowEmpty('verse_text_hebrew');

        $validator
            ->scalar('verse_text')
            ->allowEmpty('verse_text');

        $validator
            ->add('mp3', [
                'uploadError' => [
                    'rule' => 'uploadError',
                    'message' => 'The mp3 upload failed.',
                    'allowEmpty' => true,
                ],

                'mimeType' => [
                    'rule' => array('mimeType', array('audio/mp3',"audio/mpeg",'audio/x-mpeg','audio/x-mp3','audio/mpeg3','audio/x-mpeg3','audio/mpg','audio/x-mpg','audio/x-mpegaudio')),
                    'message' => 'Please only upload mp3 files',
                    'allowEmpty' => true,
                ],

            ]);
        $validator->allowEmpty('mp3');
        $validator->allowEmpty('dir');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
