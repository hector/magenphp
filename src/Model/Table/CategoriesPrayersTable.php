<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;
/**
 * CategoriesPrayers Model
 *
 * @property \App\Model\Table\CategoriesTable|\Cake\ORM\Association\BelongsTo $Categories
 * @property \App\Model\Table\PrayersTable|\Cake\ORM\Association\BelongsTo $Prayers
 *
 * @method \App\Model\Entity\CategoriesPrayer get($primaryKey, $options = [])
 * @method \App\Model\Entity\CategoriesPrayer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CategoriesPrayer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CategoriesPrayer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CategoriesPrayer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CategoriesPrayer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CategoriesPrayer findOrCreate($search, callable $callback = null, $options = [])
 */
class CategoriesPrayersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('categories_prayers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Prayers', [
            'foreignKey' => 'prayer_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('position')
            ->allowEmpty('position');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        $rules->add($rules->existsIn(['prayer_id'], 'Prayers'));

        return $rules;
    }

    function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options){
        echo "";
    }

}
