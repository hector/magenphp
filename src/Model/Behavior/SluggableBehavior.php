<?php
namespace App\Model\Behavior;
use Cake\Utility\Text;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;

class SluggableBehavior extends Behavior
{

    protected $_defaultConfig = [
        'field' => 'title',
        'slug' => 'slug',
        'replacement' => '-',
        'implementedFinders' => [
            'slugged' => 'findSlug',
        ]
    ];

    public function initialize(array $config)
    {

    }
    public function slug(Entity $entity)
    {
        $config = $this->getConfig();
        $value = $entity->get($config['field']);
        $slug =  Text::slug($value, $config['replacement']);
        $slug = strtolower($slug);
        $i = 0;
        $params = array ();
        $params ['conditions']= [];
        $params ['conditions'][$this->getTable()->getAlias().'.slug'] = $slug;
        if($entity->get("id")){
            $params ['conditions'][$this->getTable()->getAlias().'.id <>'] = $entity->get("id");
        }
        while ($this->getTable()->find('all',$params)->count()) {

            if (!preg_match ('/-{1}[0-9]+$/', $slug )) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
            }
            $params ['conditions'][$this->getTable()->getAlias() . '.slug'] = $slug;
        }

        $entity->set($config['slug'],$slug);
    }

    public function beforeSave(Event $event,EntityInterface $entity)
    {
        if($entity->isNew()){
            $this->slug($entity);
        }

    }

    public function findSlug(Query $query,$options)
    {
        return $query->where(['slug' => $options['slug']]);
    }

}