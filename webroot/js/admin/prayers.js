$(function(){
    if(typeof(CKEDITOR) == "object"){
        CKEDITOR.replace( 'prayers-description' );
        CKEDITOR.replace( 'prayers-text' );
        CKEDITOR.replace( 'prayers-transliteration' );
        CKEDITOR.replace( 'prayers-explanation' );
        CKEDITOR.config.font_names = 'Stam_Normal;David_Normal;Frank_Normal;Keren_Normal;Rashi_Normal;Aram_Normal;Miryam_Normal;' + CKEDITOR.config.font_names;
        CKEDITOR.config.contentsCss = [ CKEDITOR.config.contentsCss, Admin.basePath+"css/admin/ckeditor.css" ];
        CKEDITOR.config.stylesSet = [
            {
                name: 'Hebrew text',
                element: 'p',
                attributes: {
                    'class': 'text-rtl',
                    'style': "direction:rtl"
                }
            },
            {
                name: 'Grey text',
                element: 'span',
                attributes: {
                    'class': 'grey-text',
                    'style': "color:#888"
                }
            },
            {
                name: 'LTR text',
                element: 'p',
                attributes: {
                    'class' :"",
                    'style': "direction:ltr"
                }
            },
            {
                name: 'Small',
                element: 'small',
                attributes: {
                    'class' :"",
                    'style': ""
                }
            }


        ];

        CKEDITOR.config.filebrowserUploadUrl = Admin.basePath+'admin/pages/upload_editor';
        //CKEDITOR.config.extraPlugins = 'uploadimage';
        CKEDITOR.config.uploadUrl = Admin.basePath+'admin/pages/upload_editor?met=drop';
        CKEDITOR.config.allowedContent = true;

    }


    if(typeof($.fn.select2) == "function"){
        $("select.select-2").select2()
    }
    if(typeof(Sortable) == "function"){

        //var categories = document.getElementById("categories");

        $(".categories-list ol").each(function(){
            var id = $(this).attr("id");
            var list = document.getElementById(id);
            Sortable.create(list, {
                onUpdate: function (/**Event*/evt) {
                    var $actualList = $(evt.to);
                    var item = $(evt.item);


                    ids = [];
                    $actualList.find("li").each(function(){
                        ids.push($(this).data("id"));
                    });

                    console.log("cat: "+$actualList.data("category-id")," ids: ",ids);

                    var _data = {
                        category_id:$actualList.data("category-id"),
                        ids:ids
                    };

                    $.ajax({
                        type: "POST",
                        url: Admin.basePath+'/admin/prayers/save_order.json',
                        data: {data:_data},
                        success: function(data){
                            if(data.result == "saved"){
                                $.bootstrapGrowl("Order saved.", { type: 'success' });
                            }else{
                                $.bootstrapGrowl("Error, order cannot be saved.", { type: 'danger' });
                            }
                            $("#save").button('reset');

                        },
                        dataType: 'json'
                    });

                }
            });
        });


       /* $('.dd').nestable({ maxDepth:1 }).on('change',save);

        $("#save").click(function(){
            $(this).button('loading');
            save();
        });
        function save(){
            var _data = $('.dd').nestable('serialize');
            $.ajax({
                type: "POST",
                url: Admin.basePath+'/admin/prayers/save_order.json',
                data: {data:_data},
                success: function(data){
                    if(data.result == "saved"){
                        $.bootstrapGrowl("Order saved.", { type: 'success' });
                    }else{
                        $.bootstrapGrowl("Error, order cannot be saved.", { type: 'danger' });
                    }
                    $("#save").button('reset');

                },
                dataType: 'json'
            });
        }*/
    }

    $(".test-link").on("click",function(){
        var type = $("#prayers-type").val();
        var link = encodeURI($("#prayers-link").val());
        var $btn = $(this);
        $btn.text("testing...").attr("disabled","disabled");
        $.get(Admin.basePath+"/admin/prayers/test-link?type="+type,"&link="+link)
            .done(function (result) {
                    console.log(result);
                    if(result != 0){
                        bootbox.alert("The link is valid");
                    }else{
                        bootbox.alert("The link is invalid");
                    }
                    $btn.text("test").removeAttr("disabled");
            }).fail(function (error) {
                    console.log(error);
            });
    });


});