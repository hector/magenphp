$(function(){


    if(typeof($.fn.nestable) == "function"){
        $('.dd').nestable({ maxDepth:1 }).on('change',save);

        $("#save").click(function(){
            $(this).button('loading');
            save();
        });
        function save(){
            var _data = $('.dd').nestable('serialize');
            $.ajax({
                type: "POST",
                url: Admin.basePath+'/admin/categories/save_order.json',
                data: {data:_data},
                success: function(data){
                    if(data.result == "saved"){
                        $.bootstrapGrowl("Order saved.", { type: 'success' });
                    }else{
                        $.bootstrapGrowl("Error, order cannot be saved.", { type: 'danger' });
                    }
                    $("#save").button('reset');

                },
                dataType: 'json'
            });
        }
    }

});