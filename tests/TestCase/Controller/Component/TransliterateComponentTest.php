<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\TransliterateComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\TransliterateComponent Test Case
 */
class TransliterateComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\TransliterateComponent
     */
    public $Transliterate;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Transliterate = new TransliterateComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Transliterate);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
