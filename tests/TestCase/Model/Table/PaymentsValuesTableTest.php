<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PaymentsValuesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PaymentsValuesTable Test Case
 */
class PaymentsValuesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PaymentsValuesTable
     */
    public $PaymentsValues;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.payments_values'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PaymentsValues') ? [] : ['className' => PaymentsValuesTable::class];
        $this->PaymentsValues = TableRegistry::get('PaymentsValues', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PaymentsValues);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
