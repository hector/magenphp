<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PrayersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PrayersTable Test Case
 */
class PrayersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PrayersTable
     */
    public $Prayers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.prayers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Prayers') ? [] : ['className' => PrayersTable::class];
        $this->Prayers = TableRegistry::get('Prayers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Prayers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
