<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CandlesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CandlesTable Test Case
 */
class CandlesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CandlesTable
     */
    public $Candles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.candles',
        'app.payments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Candles') ? [] : ['className' => CandlesTable::class];
        $this->Candles = TableRegistry::get('Candles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Candles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
