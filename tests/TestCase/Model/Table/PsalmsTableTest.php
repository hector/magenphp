<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PsalmsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PsalmsTable Test Case
 */
class PsalmsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PsalmsTable
     */
    public $Psalms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.psalms'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Psalms') ? [] : ['className' => PsalmsTable::class];
        $this->Psalms = TableRegistry::get('Psalms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Psalms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
